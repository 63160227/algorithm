import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class file {

    public static int convertStringToInt(String S) {
        int sum = 0;
        for (int i = 0; i < S.length(); i++) {
            sum *= 10;
            sum += S.charAt(i) - '0';
        }
        return sum;
    }
    public static void main(String[] args) {

        try {
            File f = new File(args[0]);
            Scanner in = new Scanner(f);
            while (in.hasNextLine()) {
                String S = in.nextLine();

                int number = convertStringToInt(S);

                System.out.println(number);
            }
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, File not found");
        }
    }

}
