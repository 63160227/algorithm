import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class file {
    public static void main(String[] args) {

        try {
            File f = new File(args[0]);
            Scanner in = new Scanner(f);
            String sum = "";
            while(in.hasNextLine()) {
                String A[] = in.nextLine().split(" ");
                int start = 0;
                int end = A.length-1;
                while(start < end) {
                    String temp = A[start];
                    A[start] = A[end];
                    A[end] = temp;
                    start++;
                    end--;
                }

                for (String n : A) {
                    sum += n+" "; 
                }
                System.out.print(sum);
            }
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
